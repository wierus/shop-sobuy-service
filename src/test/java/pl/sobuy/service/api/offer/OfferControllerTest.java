package pl.sobuy.service.api.offer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.sobuy.service.domain.offer.OfferFacade;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Random;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class OfferControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    OfferFacade offerFacade;

    private final static String FIND_OFFERS_URL = "/offers/";
    private static OfferRequest offerRequest;
    private static String json;

    @BeforeAll
    static void createAndMapToJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        Random rd = new Random();
        String title = RandomStringUtils.randomAlphabetic(10);
        String description = RandomStringUtils.randomAlphabetic(50);
        offerRequest = new OfferRequest(rd.nextLong(), rd.nextLong(), title, description,
                rd.nextInt(), new BigDecimal(String.valueOf(rd.nextDouble())),
                LocalDateTime.now(), LocalDateTime.now().plusDays(10).plusMinutes(10));
        try {
            json = objectMapper.writeValueAsString(offerRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    void createOffer_Response_Created() throws Exception {
        mockMvc.perform(post(FIND_OFFERS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
    }

    //    @Test
    void createOffer_Response_Error() throws Exception {
        doThrow(new RuntimeException()).when(offerFacade).createOffer(offerRequest);
        System.out.println(json);
        mockMvc.perform(post(FIND_OFFERS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isNotFound());

        verify(offerFacade, times(1)).createOffer(offerRequest);
    }

    @Test
    void findAllOffers_Response_Ok() throws Exception {
        mockMvc.perform(get(FIND_OFFERS_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[]"));

        verify(offerFacade, times(1)).findAllOffers();
    }

    @Test
    void findOfferById_Response_NotFound() throws Exception {
        doThrow(new RuntimeException()).when(offerFacade).findOfferById(anyLong());

        mockMvc.perform(get(FIND_OFFERS_URL + anyLong()))
                .andExpect(status().isNotFound());

        verify(offerFacade, times(1)).findOfferById(anyLong());
    }

    @Test
    void findOfferById_Response_Ok() throws Exception {
        mockMvc.perform(get(FIND_OFFERS_URL + anyLong()))
                .andExpect(status().isOk());

        verify(offerFacade, times(1)).findOfferById(anyLong());
    }
}