package pl.sobuy.service.infrastructure.offer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sobuy.service.domain.offer.Offer;

@Repository
interface OfferRepository extends JpaRepository<Offer, Long> {
}
