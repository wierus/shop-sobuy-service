package pl.sobuy.service.infrastructure.offer;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.sobuy.service.domain.offer.Offer;
import pl.sobuy.service.domain.offer.OfferRetrievalClient;

import java.util.List;

@Service
@RequiredArgsConstructor
class OfferRetrievalPostgresClient implements OfferRetrievalClient {

    private final OfferRepository offerRepository;

    @Override
    public List<Offer> findAll() {
        return offerRepository.findAll();
    }

    @Override
    public Offer findById(long id) {
        return offerRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cannot find offer with id = " + id));
    }
}
