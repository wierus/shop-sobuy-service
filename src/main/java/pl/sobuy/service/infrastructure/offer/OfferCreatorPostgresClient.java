package pl.sobuy.service.infrastructure.offer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sobuy.service.domain.offer.Offer;
import pl.sobuy.service.domain.offer.OfferCreatorClient;

@Service
@RequiredArgsConstructor
class OfferCreatorPostgresClient implements OfferCreatorClient {

    private final OfferRepository offerRepository;

    @Override
    public void save(Offer offer) {
        offerRepository.save(offer);
    }
}
