package pl.sobuy.service.infrastructure.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sobuy.service.domain.order.Order;

@Repository
interface OrderRepository extends JpaRepository<Order, Long> {
}
