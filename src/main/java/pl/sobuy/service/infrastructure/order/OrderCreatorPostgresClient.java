package pl.sobuy.service.infrastructure.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sobuy.service.domain.order.OrderCreatorClient;
import pl.sobuy.service.domain.order.Order;

@Service
@RequiredArgsConstructor
class OrderCreatorPostgresClient implements OrderCreatorClient {

    private final OrderRepository orderRepository;

    @Override
    public void create(Order order) {
        orderRepository.save(order);
    }
}
