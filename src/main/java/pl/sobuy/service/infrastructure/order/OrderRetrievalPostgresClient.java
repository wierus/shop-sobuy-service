package pl.sobuy.service.infrastructure.order;

import lombok.RequiredArgsConstructor;
import org.hibernate.mapping.Collection;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.sobuy.service.domain.order.Order;
import pl.sobuy.service.domain.order.OrderRetrievalClient;
import pl.sobuy.service.domain.order.OrderStatus;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class OrderRetrievalPostgresClient implements OrderRetrievalClient {

    private final OrderRepository orderRepository;

    @Override
    public Order findById(long id) {
        return orderRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Order with id = %s not found", id)));
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> findPending() {
        return orderRepository.findAll().stream()
                .filter(x -> x.getStatus() == OrderStatus.PENDING)
                .collect(Collectors.toList());
    }
}
