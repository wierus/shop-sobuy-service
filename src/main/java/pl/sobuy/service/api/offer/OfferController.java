package pl.sobuy.service.api.offer;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sobuy.service.domain.offer.Offer;
import pl.sobuy.service.domain.offer.OfferFacade;

import java.util.List;

@RestController
@RequestMapping("/offers")
@RequiredArgsConstructor
class OfferController {

    private final OfferFacade offerFacade;

    @PostMapping
    public ResponseEntity<HttpStatus> create(@RequestBody OfferRequest offerRequest) {
        offerFacade.createOffer(offerRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public List<Offer> findAll() {
        return offerFacade.findAllOffers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Offer> findById(@PathVariable long id) {
        try {
            return ResponseEntity.ok(offerFacade.findOfferById(id));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
