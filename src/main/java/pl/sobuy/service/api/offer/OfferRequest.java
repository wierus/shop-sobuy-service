package pl.sobuy.service.api.offer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OfferRequest {

    private long ownerId;

    //    TODO changed from id to number, check if below everything is fine and switch to String type
    private long ownerAccountNumber;

    private String title;

    private String description;

    private int quantity;

    private BigDecimal price;

    private LocalDateTime startDate;

    private LocalDateTime endDate;
}
