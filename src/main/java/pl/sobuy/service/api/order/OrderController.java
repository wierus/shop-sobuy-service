package pl.sobuy.service.api.order;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sobuy.service.domain.order.Order;
import pl.sobuy.service.domain.order.OrderFacade;
import pl.sobuy.service.domain.payment.Payment;

import javax.validation.constraints.Min;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
class OrderController {

    private final OrderFacade orderFacade;

    @PostMapping
    public ResponseEntity<HttpStatus> create(@RequestBody OrderRequest orderRequest) {
        orderFacade.createOrder(orderRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public List<Order> findAll() {
        return orderFacade.findAllOrders();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> findById(@PathVariable @Min(1) long id) {
        return ResponseEntity.ok(orderFacade.findOrderById(id));
    }

    @GetMapping("/pending")
    public ResponseEntity<List<Payment>> findPending() {
        return ResponseEntity.ok((orderFacade.findPendingOrders().stream()
                .map(Payment::generate)
                .collect(Collectors.toList())));
    }

    @PostMapping("/pending/{id}")
    public void setOrderStatus(@PathVariable @Min(1) long id) {
        orderFacade.setOrderStatusToPaid(id);
    }
}
