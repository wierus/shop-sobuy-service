package pl.sobuy.service.api.order;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class OrderRequest {

    private long offerId;

    private long clientId;

    private String clientAccountNumber;

    private int quantity;
}
