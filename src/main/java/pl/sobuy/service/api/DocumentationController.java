package pl.sobuy.service.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class DocumentationController {

    @GetMapping("/")
    public void documentation(HttpServletResponse response) throws IOException {
        response.sendRedirect("/swagger-ui/");
    }

}
