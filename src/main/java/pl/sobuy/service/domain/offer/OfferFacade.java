package pl.sobuy.service.domain.offer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sobuy.service.api.offer.OfferRequest;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OfferFacade {

    private final OfferCreatorClient createClient;
    private final OfferRetrievalClient retrievalClient;

    @Transactional
    public void createOffer(OfferRequest offerRequest) {
        Offer offer = Offer.create(offerRequest);
        createClient.save(offer);
    }

    public List<Offer> findAllOffers() {
        return retrievalClient.findAll();
    }

    public Offer findOfferById(long offerId) {
        return retrievalClient.findById(offerId);
    }

}
