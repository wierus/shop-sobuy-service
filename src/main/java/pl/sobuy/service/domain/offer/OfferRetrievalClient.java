package pl.sobuy.service.domain.offer;

import java.util.List;

public interface OfferRetrievalClient {

    List<Offer> findAll();

    Offer findById(long id);
}
