package pl.sobuy.service.domain.offer;

public interface OfferCreatorClient {

    void save(Offer offer);
}
