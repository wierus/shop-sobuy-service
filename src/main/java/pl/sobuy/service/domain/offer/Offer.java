package pl.sobuy.service.domain.offer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Transient;
import pl.sobuy.service.api.offer.OfferRequest;
import pl.sobuy.service.domain.Auditable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table
public class Offer extends Auditable {

    @Setter(value = AccessLevel.PRIVATE)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @SequenceGenerator(sequenceName = "offer_id", name = "sequence_offer")
    private Long id;

    @Setter(value = AccessLevel.PRIVATE)
    @Column(name = "owner_id")
    private Long ownerId;

    @Setter(value = AccessLevel.PRIVATE)
    @Column(name = "owner_account_number")
    private long ownerAccountNumber;

    private String title;

    @Column(length = 1500)
    private String description;

    private Integer quantity;
    private BigDecimal price;

    @Column(name = "start_date")
    private LocalDateTime startDate;
    @Column(name = "end_date")
    private LocalDateTime endDate;

    public void subtractQuantity(int orderedQuantity) {
        if (orderedQuantity > quantity) {
            throw new IllegalArgumentException("Ordered quantity exceed available quantity of product");
        }
        this.quantity -= orderedQuantity;
    }

    public void verifyActivity() {
        if (LocalDateTime.now().isBefore(startDate) || LocalDateTime.now().isAfter(endDate)) {
            throw new IllegalArgumentException("Offer is closed");
        }
    }

    public static Offer create(OfferRequest offerRequest) {
        Offer offer = new Offer();
        offer.setOwnerId(offerRequest.getOwnerId());
        offer.setOwnerAccountNumber(offerRequest.getOwnerAccountNumber());
        offer.setTitle(offerRequest.getTitle());
        offer.setDescription(offerRequest.getDescription());
        offer.setQuantity(offerRequest.getQuantity());
        offer.setPrice(offerRequest.getPrice());
        offer.setStartDate(offerRequest.getStartDate());
        offer.setEndDate(offerRequest.getEndDate());
        return offer;
    }

    public String getStartDateString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        return startDate.format(formatter);
    }

    public String getEndDateString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        return endDate.format(formatter);
    }
}
