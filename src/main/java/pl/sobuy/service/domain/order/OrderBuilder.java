package pl.sobuy.service.domain.order;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sobuy.service.api.order.OrderRequest;
import pl.sobuy.service.domain.offer.Offer;
import pl.sobuy.service.domain.offer.OfferRetrievalClient;
import pl.sobuy.service.domain.payment.PaymentSenderClient;

@Slf4j
@Service
@RequiredArgsConstructor
class OrderBuilder {

    private final OfferRetrievalClient offerRetrievalClient;
    private final OrderCreatorClient orderCreatorClient;
    private final PaymentSenderClient paymentSenderClient;

    @Transactional
    public Order build(OrderRequest orderRequest) {
        Offer offer = offerRetrievalClient.findById(orderRequest.getOfferId());
        offer.verifyActivity();
        Order order = Order.create(orderRequest, offer);
        offer.subtractQuantity(order.getQuantity());
        orderCreatorClient.create(order);
        return order;
    }

    @Transactional
    public void sendPaymentAndChangeStatus(Order order) {
        try {
            paymentSenderClient.sendPayment(order);
            order.setStatusToPaid();
        } catch (Exception ex) {
            log.info("Exception thrown: " + ex.getMessage());
        }
    }
}
