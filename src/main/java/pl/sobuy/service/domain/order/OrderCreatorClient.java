package pl.sobuy.service.domain.order;

public interface OrderCreatorClient {

    void create(Order order);
}
