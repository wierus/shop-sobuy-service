package pl.sobuy.service.domain.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sobuy.service.api.order.OrderRequest;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderFacade {

    private final OrderRetrievalClient orderRetrievalClient;
    private final OrderBuilder orderBuilder;

    public void createOrder(OrderRequest orderRequest) {
        Order order = orderBuilder.build(orderRequest);
        orderBuilder.sendPaymentAndChangeStatus(order);
    }

    public List<Order> findAllOrders() {
        return orderRetrievalClient.findAll();
    }

    public Order findOrderById(long orderId) {
        return orderRetrievalClient.findById(orderId);
    }

    public List<Order> findPendingOrders() {
        return orderRetrievalClient.findPending();
    }

    public void setOrderStatusToPaid(long orderId) {
        findOrderById(orderId).setStatusToPaid();
    }
}
