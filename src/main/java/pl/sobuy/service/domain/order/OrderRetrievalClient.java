package pl.sobuy.service.domain.order;

import java.util.List;

public interface OrderRetrievalClient {

    Order findById(long id);

    List<Order> findAll();

    List<Order> findPending();
}
