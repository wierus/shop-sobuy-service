package pl.sobuy.service.domain.order;

public enum OrderStatus {
    PENDING, PAID
}
