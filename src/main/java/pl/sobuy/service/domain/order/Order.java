package pl.sobuy.service.domain.order;

import lombok.*;
import pl.sobuy.service.api.order.OrderRequest;
import pl.sobuy.service.domain.Auditable;
import pl.sobuy.service.domain.offer.Offer;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter(value = AccessLevel.PRIVATE)
@Table(name = "ORDERS")
public class Order extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "order_sequence")
    @SequenceGenerator(name = "order_sequence")
    private long id;

    @Column(name = "owner_id")
    private long ownerId;
    @Column(name = "owner_account_number")
    private long ownerAccountNumber;
    @Column(name = "offer_id")
    private long offerId;
    @Column(name = "client_id")
    private long clientId;
    @Column(name = "client_account_number")
    private String clientAccountNumber;

    private int quantity;

    @Column(name = "unit_price")
    private BigDecimal unitPrice;
    @Enumerated(EnumType.STRING)
    private OrderStatus status = OrderStatus.PENDING;

    public static Order create(OrderRequest orderRequest, Offer offer) {
        Order order = new Order();
        order.setOwnerId(offer.getOwnerId());
        order.setOwnerAccountNumber(offer.getOwnerAccountNumber());
        order.setOfferId(orderRequest.getOfferId());
        order.setClientId(orderRequest.getClientId());
        order.setClientAccountNumber(orderRequest.getClientAccountNumber());
        order.setQuantity(orderRequest.getQuantity());
        order.setUnitPrice(offer.getPrice());
        return order;
    }

    public void setStatusToPaid() {
        this.setStatus(OrderStatus.PAID);
    }

    public BigDecimal calculateTotalPrice() {
        return this.unitPrice.multiply(new BigDecimal(this.quantity));
    }
}
