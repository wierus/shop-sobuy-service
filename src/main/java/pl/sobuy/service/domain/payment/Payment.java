package pl.sobuy.service.domain.payment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.sobuy.service.domain.order.Order;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Payment {

    private long id;
    private long ownerId;
    private long ownerAccountNumber;
    private String clientAccountNumber;
    private BigDecimal totalPrice;
    private String title;

    public static Payment generate(Order order) {
        Payment payment = new Payment();
        payment.id = order.getId();
        payment.ownerId = order.getOwnerId();
        payment.ownerAccountNumber = order.getOwnerAccountNumber();
        payment.clientAccountNumber = order.getClientAccountNumber();
        payment.totalPrice = order.getUnitPrice().multiply(new BigDecimal(order.getQuantity()));
        payment.title = String.format("OrderId = %s, ownerId = %s, clientId = %s",
                order.getId(), order.getOwnerId(), order.getClientId());
        return payment;
    }
}
