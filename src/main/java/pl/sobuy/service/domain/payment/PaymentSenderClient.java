package pl.sobuy.service.domain.payment;


import pl.sobuy.service.domain.order.Order;

public interface PaymentSenderClient {

    void sendPayment(Order order);
}

