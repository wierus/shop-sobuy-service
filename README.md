# Service for shop-sobuy project

Shop SoBuy is a platform for making sales offer, order and settlements of payments.
Project uses Spring Data JPA, Web to serve request from frontend layer ([shop-sobuy-frontend](https://gitlab.com/wierus/shop-sobuy-frontend)), Swagger to provide documentation and Jenkins for Continous Delivery

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to have installed in order to run project locally

* Java 8
```
To run application go to ServiceApplication class and start it 
```

## Heroku

Project is available on heroku [here](https://sobuy-service.herokuapp.com/swagger-ui/).

## Authors

* **Patryk Stanek**  
e-mail: *pstanek18@gmail.com*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details